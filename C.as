package
{
	public class C
	{
		public static const SCREEN_WIDTH:int = 1440;
		public static const SCREEN_HEIGHT:int = 600;
		public static const SCREEN_RESOLUTION:Number = SCREEN_WIDTH / SCREEN_HEIGHT;
		public static const FPS:Number = 60;
		public static const MAN_WIDTH:int = 12;
		public static const MAN_HEIGHT:int = 30;
		public static const BLOCK_WIDTH:int = 36;
		public static const BLOCK_HEIGHT:int = 12;
		public static const BLOCKS_WIDE:int = SCREEN_WIDTH / BLOCK_WIDTH;
		public static const BLOCKS_HIGH:int = SCREEN_HEIGHT / BLOCK_HEIGHT;
		public static const STAGE_HEIGHT:int = 3*BLOCKS_HIGH;
		public static const MUHNIEZ_WIDTH:int = 32;
		public static const MUHNIEZ_HEIGHT:int = 16;
		public static const MUHNIEZ_PROB:Number = 0.6;
		public static const BLOWER_WIDTH:int = 36;
		public static const BLOWER_HEIGHT:int = 24;
		public static const BLOWER_LENGTH:int = STAGE_HEIGHT*BLOCK_HEIGHT - (BLOCK_HEIGHT*2);
		public static const MAX_EXPLOSIONS:int = 2;
	} 
}