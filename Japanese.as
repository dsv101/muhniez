package
{
	import net.flashpunk.FP;

	public class Japanese
	{
		public static var source:String = "";
		public static var lines:Array = new Array("");

		public static function init(embed:Class):void
		{
			source = new embed();
			lines = source.split("。");
		}

		public static function random():String
		{
			var i:int = FP.rand(lines.length);

			return lines[i];
		}
	}
}