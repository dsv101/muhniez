package
{
	public class A
	{
		[Embed(source = "img/title.png")]
		public static const TITLE:Class;

		[Embed(source = "img/man.png")]
		public static const MAN:Class;

		[Embed(source = "img/muhniez.png")]
		public static const MUHNIEZ:Class;

		[Embed(source = "img/block.png")]
		public static const BLOCK:Class;

		[Embed(source = "img/blower.png")]
		public static const BLOWER:Class;

		[Embed(source = "japanese.txt", mimeType="application/octet-stream")]
		public static const CHING_CHONG:Class;

		[Embed(source = "snd/applause.mp3")]
		public static const APPLAUSE:Class;

		[Embed(source = "snd/explosion.mp3")]
		public static const EXPLOSION:Class;

		[Embed(source = "snd/flying.mp3")]
		public static const FLYING:Class;

		[Embed(source = "snd/song1.mp3")]
		public static const SONG1:Class;

		[Embed(source = "snd/song2.mp3")]
		public static const SONG2:Class;

		[Embed(source = "snd/squeak.mp3")]
		public static const SQUEAK:Class;

		[Embed(source = "snd/float.mp3")]
		public static const FLOAT:Class;
	}
}