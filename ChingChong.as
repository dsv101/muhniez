package
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.Tween;
	import net.flashpunk.FP;

	public class ChingChong extends Entity
	{
		private var gl:Graphiclist = new Graphiclist();
		private var lifespan:Number = 2;
		private var o:Number = 0;

		public function ChingChong(txt:String, x:Number=0, y:Number=0, size:Number=100, gap:Number=4, lifespan:Number=4, useColor:Boolean=true, fadeSize:Boolean=true)
		{
			this.lifespan=lifespan;
			super(x, y, gl, null);
			layer=-1;

			var ss:Number = size;

			for each (var c:String in txt.split(''))
			{
				if (c == "ー")
					c = "|";
				var t:Text = new Text(c,0,o,{size:size, align:"center", alpha:(size/ss), color:((useColor) ? V.randomColor() : 0xFFFFFF)});
				if (c == "|")
					t.x+=size/2 - t.width/2;
				gl.add(t);
				o+=t.height+gap;
				if (fadeSize)
				{
					size *= 0.9;
					gap *= 0.9;
				}
			} 
		}

		override public function added():void
		{
			if (lifespan != -1)
				FP.tween(this, {y:(y-3000)}, lifespan, {complete:destroy, type:Tween.ONESHOT});
		}

		private function destroy():void
		{
			FP.world.remove(this);
		}

		public function getHeight():Number
		{
			return o;
		}
	}
}