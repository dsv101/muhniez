package
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Tween;
	import net.flashpunk.FP;

	public class Muhniez extends Entity
	{
		private const OBTAIN_TIME:uint = 2;

		private var spriteMap:Spritemap = new Spritemap(A.MUHNIEZ, C.MUHNIEZ_WIDTH, C.MUHNIEZ_HEIGHT);
		private var obtaining:Boolean = false;
		private var sx:Number = 0;
		private var sy:Number = 0;
		private var waiting:Boolean = false;
		private var destScale:Number = 1;

		public function Muhniez(x:Number=0, y:Number=0, isCaptured:Boolean=false)
		{
			width = C.MUHNIEZ_WIDTH;
			height = C.MUHNIEZ_HEIGHT;
			centerOrigin();
			type = "muhniez";
			layer = 10;

			spriteMap.centerOrigin();
			spriteMap.add("still", [0]);
			spriteMap.add("wave", [0, 1, 2, 3, 4, 5, 4, 3, 2, 1], 25);
			spriteMap.play((isCaptured) ? "still" : "wave");

			sx = x;
			sy = y;
			super(x, y, spriteMap, null);
		}

		override public function update():void
		{
			if (waiting && collide("man", sx, sy) == null)
			{
				reset();
			}
			spriteMap.color = V.col4;
			updateAngle();
		}

		override public function render():void
		{
			super.render();
		}

		public function updateAngle():void
		{
			if (obtaining || FP.distance(x, y, V.manX, V.manY) > 50)
				return;

			if (FP.random <= C.MUHNIEZ_PROB)
				spriteMap.angle += V.n1;
		}

		public function obtain():void
		{
			if (obtaining)
				return;

			destScale = 1 + V.getSeed();
			FP.tween(spriteMap, {scaleX:destScale, scaleY:destScale}, OBTAIN_TIME+0.1, {complete:waitForReset, type:Tween.ONESHOT});
			FP.tween(spriteMap, {alpha:0}, OBTAIN_TIME, {type:Tween.ONESHOT});
			FP.tween(this, {x:C.SCREEN_WIDTH/2, y:C.SCREEN_HEIGHT/2}, OBTAIN_TIME, {type:Tween.ONESHOT});

			obtaining = true;
		}

		public function isObtaining():Boolean
		{
			return obtaining;
		}

		private function waitForReset():void
		{
			waiting=true;
		}

		private function reset():void
		{
			waiting = false;
			obtaining = false;
			x = sx;
			y = sy;
			spriteMap.scaleX = 1;
			spriteMap.scaleY = 1;
			spriteMap.alpha = 1;
		}
	}
}