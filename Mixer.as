package
{
	import net.flashpunk.Sfx;
	import net.flashpunk.FP;
	import net.flashpunk.Tween;
	import flash.utils.Dictionary;

	public class Mixer
	{
		public static const APPLAUSE:Sfx = new Sfx(A.APPLAUSE);
		public static const EXPLOSION:Sfx = new Sfx(A.EXPLOSION);
		public static const FLYING:Sfx = new Sfx(A.FLYING);
		public static const SONG1:Sfx = new Sfx(A.SONG1);
		public static const SONG2:Sfx = new Sfx(A.SONG2);
		public static const SQUEAK:Sfx = new Sfx(A.SQUEAK);
		public static const FLOAT:Sfx = new Sfx(A.FLOAT);

		private static var explosions:Vector.<Sfx> = new Vector.<Sfx>();
		private static var fading:Dictionary = new Dictionary();

		public function Mixer()
		{
			
		}

		public static function fadeIn(source:*, length:Number=1, loop:Boolean=true):void
		{
			var sfx:Sfx = null;
			if (source is Class)
			{
				sfx = new Sfx(source);
			}
			else if (source is Sfx)
			{
				sfx = source
			}
			else
			{
				return;
			}

			if (sfx.playing)
				return;

			if (loop)
			{
				sfx.loop();
			}
			else
			{
				sfx.play();
			}

			sfx.volume = 1;
			if (length>0)
			{
				sfx.volume = 0;
				FP.tween(sfx, {volume:1}, length, {type:Tween.ONESHOT});
			}
		}

		public static function fadeOut(sfx:Sfx, length:Number=1):void
		{
			if (!sfx.playing)
				return;

			if (length<=0)
			{
				sfx.stop();
				return;
			}

			fading[sfx] = true;
			function reset():void {sfx.stop(); sfx.volume=1; fading[sfx] = false;}
			FP.tween(sfx, {volume:0}, length, {type:Tween.ONESHOT, complete:reset});
		}

		public static function isFading(sfx:Sfx):Boolean
		{
			return fading[sfx] as Boolean;
		}

		public static function explode():void
		{
			if (explosions.length != C.MAX_EXPLOSIONS)
			{
				for (var i:int = 0; i < C.MAX_EXPLOSIONS; ++i)
				{
					explosions.push(new Sfx(A.EXPLOSION));
				}
			}

			var last:Sfx = null;
			var max:Number = 0;

			for each (var e:Sfx in explosions)
			{
				if (!e.playing)
				{
					e.play();
					break;
				}
				else
				{
					if (e.position > max)
					{
						last = e;
						max = e.position;
					}
				}
			}

			if (last != null)
			{
				e.stop();
				e.play();
			}
		}
	}
}