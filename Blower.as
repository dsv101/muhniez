package
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.Graphic;

	public class Blower extends Entity
	{
		private var gl:Graphiclist = new Graphiclist;
		private var g:Graphic = null

		public function Blower(x:Number=0, y:Number=0, reps:uint=1)
		{
			width = C.BLOWER_WIDTH;
			height = C.BLOWER_LENGTH;
			originX = C.BLOWER_WIDTH/2;
			originY = C.BLOWER_LENGTH;
			type = "blower";

			super(x, y, gl, null);

			for (var i:int = 0; i < reps; ++i)
			{
				var spr:Spritemap = new Spritemap(A.BLOWER, C.BLOWER_WIDTH, C.BLOWER_HEIGHT);
				spr.originX = C.BLOWER_WIDTH/2;
				spr.originY = C.BLOWER_HEIGHT;
				spr.add("blowing", [2, 3, 4, 3, 2, 1, 2], 8 + FP.rand(15));
				spr.play("blowing");
				gl.add(spr);
			}
		}

		override public function render():void
		{
			for each (g in gl.children)
			{
				(g as Spritemap).color = V.col4;
			}

			super.render();
		}
	}
}