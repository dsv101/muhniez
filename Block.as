package
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;

	public class Block extends Entity
	{
		private var image:Image;

		public function Block(x:Number=0, y:Number=0)
		{
			type = "level";
			width = C.BLOCK_WIDTH;
			height = C.BLOCK_HEIGHT;

			image = new Image(A.BLOCK);

			super(x, y, image, null);
		}

		override public function render():void
		{
			image.color = V.col3;
			super.render();
		}
	}
}