package
{
	import net.flashpunk.Engine;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
   
	public class Main extends Engine
	{
		private var gameWorld:GameWorld = null;
		private var menuWorld:MenuWorld = new MenuWorld();

		public function Main()
		{
			super(C.SCREEN_WIDTH, C.SCREEN_HEIGHT, C.FPS, false);

			Japanese.init(A.CHING_CHONG);
			gameWorld = new GameWorld();

			Input.define("left", Key.LEFT, Key.A);
			Input.define("right", Key.RIGHT, Key.D);
			Input.define("up", Key.UP, Key.W);
			Input.define("down", Key.DOWN, Key.S);
			Input.define("jump", Key.X, Key.SPACE, Key.UP);
			Input.define("action", Key.Z, Key.ENTER);

			FP.world = menuWorld;
			//FP.console.enable();
			//FP.console.visible = false;

			Mixer.fadeIn(Mixer.SONG1,0.3);
		}

		override public function update():void
		{
			/*if (Input.pressed(Key.I))
			{
				FP.console.visible = !FP.console.visible;
			}*/
			
			if (Input.pressed(Key.SPACE) && FP.world == menuWorld)
			{
				FP.world = gameWorld;
				Mixer.APPLAUSE.play();
			}

			super.update();
		}

		override public function render():void
		{
			V.update(gameWorld.getScore());
			super.render();
		}
	}
}