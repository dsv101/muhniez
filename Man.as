package
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.utils.Input;
	import net.flashpunk.FP;

	public class Man extends Entity
	{
		private const GRAV_HIGH:Number = 24;
		private const GRAV_LOW:Number = 12;
		private const ACC_SPD:Number = 30;
		private const DEC_SPD:Number = 35;
		private const DEC_SPD_TERM:Number = 40;
		private var JMP_SPD:Number = 200;
		private const MAX_HSP:Number = 80;
		private var MAX_VSP:Number = 400;
		private var MAX_HSP_BLOW:Number = 120;
		private var MAX_VSP_BLOW:Number = 1200;

		private var spriteMap:Spritemap;

		private var aSpd:Number = ACC_SPD;
		private var dSpd:Number = DEC_SPD;
		private var hsp:Number = 0;
		private var vsp:Number = 0;
		private var maxhsp:Number = MAX_HSP;
		private var maxvsp:Number = MAX_VSP;
		private var grav:Number = GRAV_HIGH;
		private var dir:int = 1;
		private var onGround:Boolean = false;
		private var moving:Boolean = false;
		private var blowing:Boolean = false;
		private var b:Blower = null;
		private var i:int = 0;
		private var vsp_whole:uint = 0;
		private var vsp_sign:int = 0
		private var hsp_whole:uint = 0;
		private var hsp_sign:int = 0;
		private var hsp_part:Number = 0;
		private var vsp_part:Number = 0;
		private var wasBlowing:Boolean = false;

		public function Man(x:Number=0, y:Number=0)
		{
			width = C.MAN_WIDTH;
			height = C.MAN_HEIGHT;
			originX = 6;
			originY = 30;
			type = "man";

			spriteMap = new Spritemap(A.MAN, 15, 30);
			spriteMap.add("still", [0]);
			spriteMap.add("walk", [1, 0, 2, 0], 6);
			spriteMap.play("still");
			spriteMap.originX = 8;
			spriteMap.originY = 30;

			super(x, y, spriteMap, null);
		}

		override public function update():void
		{
			wasBlowing = blowing;

			onGround = (collide("level", x, y + 1) != null);
			i = (collide("level", x-1, y) != null) ? 1 : ((collide("level",x+1,y) != null) ? -1 : 0);
			if (!onGround  && i != 0)
			{
				if (!Mixer.APPLAUSE.playing)
					Mixer.APPLAUSE.play();
				hsp = i * JMP_SPD;
			}

		 	//maxhsp = (blowing) ? MAX_HSP_BLOW : MAX_HSP;
		 	//maxvsp = (blowing) ? MAX_VSP_BLOW : MAX_VSP;

			handleInput();
			updatePhysics();
			animate();

			if (Mixer.FLYING.playing)
			{
				if (!Mixer.isFading(Mixer.FLYING) && vsp >= 60)
				{
					Mixer.fadeOut(Mixer.FLYING,0.2);
				}
			}

			super.update();
		}

		override public function render():void
		{
			spriteMap.color = V.col4;
			super.render();
		}

		public function blow(amt:Number=40):void
		{
			blowing = true;
			vsp -= amt;
		}

		public function isFalling():Boolean
		{
			return (onGround == false && vsp > 0);
		}

		public function isOnGround():Boolean
		{
			return onGround;
		}

		public function speedUp():void
		{
			aSpd *= 1.07;
			dSpd *= 1.07;
			maxhsp *= 1.03;
			JMP_SPD *=1.09;
			MAX_HSP_BLOW *= 1.01;
			MAX_VSP_BLOW *= 1.01;
			MAX_VSP *= 1.01;
		}

		private function handleInput():void
		{
			//Gravity
			grav = GRAV_LOW;
			if (!onGround && !blowing)
			{
				if (Input.check("jump") && vsp < 0)
					grav = GRAV_LOW;
				else
					grav = GRAV_HIGH;

				vsp += grav;

				//if (Input.released("jump") && vsp < 0)
				//	vsp = 0;
			}
			else
			{
				vsp += (!blowing) ? grav : grav*0.8;
			}
			
			//Jumping
			if (Input.pressed("jump") && onGround)
			{
				Mixer.SQUEAK.play();
				vsp = -JMP_SPD;
			}

			b = collide("blower", x, y) as Blower;
			if (b != null)
			{
				if (onGround || vsp > 20)
					blowing = false;

				if (!blowing && onGround)
				{
					vsp = -JMP_SPD*1.5;
					Mixer.FLYING.play();
				}
				var dist:Number = Math.abs(y-b.y);
				var amt:Number = ((C.BLOWER_LENGTH-dist+10)*0.006);
				blow(amt);
				if (!Mixer.FLOAT.playing && !Mixer.FLYING.playing)
					Mixer.FLOAT.loop(0.3);
			}
			else
			{
				blowing = false;
				Mixer.FLOAT.stop();
			}

			//Acceleration
			moving = true;
			if (Input.check("left"))
			{
				dir = -1;
				if (hsp < -maxhsp)
				{
					hsp += DEC_SPD_TERM;
					if (hsp > -maxhsp)
						hsp = -maxhsp;
				}
				else if (collide("level", x - 1, y) == null)
					hsp -= ACC_SPD;
				else
					moving = false;
			}
			else if (Input.check("right"))
			{
				dir = 1;
				if (hsp > maxhsp)
				{
					hsp -= DEC_SPD_TERM;
					if (hsp < maxhsp)
						hsp = maxhsp;
				}
				else if (collide("level", x + 1, y) == null)
					hsp += ACC_SPD;
				else
					moving = false;
			}
			else
			{
				//Deceleration
				moving = false;
				
				if (hsp > 0)
				{
					hsp -= dSpd;
					if (hsp < 0) hsp = 0;
				}
				if (hsp < 0)
				{
					hsp += dSpd;
					if (hsp > 0) hsp = 0;
				}
			}
		}

		private function updatePhysics():void
		{
			if (hsp > maxhsp)
				hsp = maxhsp;
			else if (hsp < -maxhsp)
				hsp = -maxhsp;

			if (vsp > maxvsp)
				vsp = maxvsp;
			else if (vsp < -maxvsp)
				vsp = -maxvsp;

			vsp_whole = 0;
			vsp_sign = FP.sign(vsp);
			hsp_whole = 0;
			hsp_sign = FP.sign(hsp);
			hsp_part = 0;
			vsp_part = 0;
			
			//Calculate pixels to move
			vsp_whole = Math.floor(Math.abs(vsp * FP.elapsed));
			vsp_part += (vsp * FP.elapsed) - (vsp_whole * vsp_sign);
			vsp_whole += Math.floor(Math.abs(vsp_part));
			vsp_part -= Math.floor(Math.abs(vsp_part)) * FP.sign(vsp_part);
			
			hsp_whole = Math.floor(Math.abs(hsp * FP.elapsed));
			hsp_part += (hsp * FP.elapsed) - (hsp_whole * hsp_sign);
			hsp_whole += Math.floor(Math.abs(hsp_part));
			hsp_part -= Math.floor(Math.abs(hsp_part)) * FP.sign(hsp_part);
			
			//Move the player
			for (i = vsp_whole ; i > 0; --i)
			{
				if (collide("level", x, y + vsp_sign) && !collide("level", x, y))
				{
					moving = false;
					vsp = 0;
					break;
				}
				else
					y += vsp_sign;
			}
			
			for (i = hsp_whole ; i > 0; --i)
			{
				if (collide("level", x + hsp_sign, y) && !collide("level", x, y))
				{
					moving = false;
					hsp = 0;
					break;
				}
				else
					x += hsp_sign;
			}
		}

		private function animate():void
		{
			if (hsp != 0)
			{
				spriteMap.play("walk");
			}
			else
			{
				spriteMap.play("still");
			}

			spriteMap.scaleX = (dir == 1) ? 1 : -1;
		}
	}
}