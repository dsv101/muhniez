package
{
	import net.flashpunk.FP;

	public class V
	{
		private static var initialized:Boolean = false;
		private static var seed:uint = 0;

		public static var col1:uint = 0xFFFFFF;
		public static var col2:uint = 0;
		public static var col3:uint = 0xFFFFFF;
		public static var col4:uint = 0xFFFFFF;

		public static var n1:Number = 0;
		public static var n2:Number = 0;
		public static var n3:Number = 0;
		
		public static var manX:Number = 0;
		public static var manY:Number = 0;

		public static function update(seed:uint):void
		{
			V.seed = seed;

			if (seed == 0)
				return;

			init();

			var prob:Number = seed/200;

			if (FP.random <= prob)
			{
				updateColors();
			}

			updateNumbers();
		}

		public static function getSeed():uint
		{
			return seed;
		}

		public static function seedFunc1(k:Number=0.1):Number
		{
			return FP.random * V.getSeed() * k * ((FP.random < 0.5) ? -1 : 1)
		}

		private static function updateColors():void
		{
			var r:uint = col1>>16 & 0xFF;
			var g:uint = col1>>8 & 0xFF;
			var b:uint = col1 & 0xFF;

			r += FP.rand(10) - FP.rand(10);
			g += FP.rand(10) - FP.rand(10);
			b += FP.rand(10) - FP.rand(10);

			r = FP.clamp(r, 0, 0xFF);
			g = FP.clamp(g, 0, 0xFF);
			b = FP.clamp(b, 0, 0xFF);

			setColors((r<<16) | (g<<8) | (b), randomColor());
		}

		private static function updateNumbers():void
		{
			n1 = seedFunc1();
			n2 = seedFunc1(0.2);
			n3 = seedFunc1(0.3);
		}

		public static function randomColor():uint
		{
			return (FP.rand(256)<<16) | (FP.rand(256)<<8) | (FP.rand(256));
		}

		private static function init():void
		{
			if (initialized)
				return;

			setColors(randomColor(), randomColor());
			initialized = true;
		}

		private static function setColors(primary:uint,secondary:uint):void
		{
			if (FP.rand(10) < 2)
				return;

			col1 = primary;
			col2 = ~primary;

			if (FP.rand(5) < 2)
				return;

			col3 = secondary;
			col4 = ~secondary;
		}
	}
}