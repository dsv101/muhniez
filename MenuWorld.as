package
{
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;

	public class MenuWorld extends World
	{
		private var chongs:Vector.<ChingChong> = new Vector.<ChingChong>();
		private var chongsOS:Vector.<ChingChong> = new Vector.<ChingChong>();
		private var title:Image = new Image(A.TITLE);
		private var msg:ChingChong = null;

		public function MenuWorld()
		{
			title.centerOrigin();
			title.x = C.SCREEN_WIDTH/2;
			title.y = C.SCREEN_HEIGHT/2;
			addGraphic(title);
		}

		private function loadChongs(num:uint):void
		{
			for (var i:uint = 0; i < num; ++i)
			{
				chongs.push(new ChingChong(Japanese.random(),FP.rand(C.SCREEN_WIDTH),C.SCREEN_HEIGHT,50+FP.rand(50),FP.rand(8),-1,false));
			}
		}

		override public function begin():void
		{
			FP.screen.color = 0x000000;
			loadChongs(80);
			msg = new ChingChong("プレススペースバー", C.SCREEN_WIDTH-100, 0, 40, 0, -1, true, false);
		}

		override public function update():void
		{
			var c:ChingChong = null;

			if (chongsOS.length < 50 && FP.random <= 0.125)
			{
				c = chongs.pop();
				c.y = C.SCREEN_HEIGHT;
				chongsOS.push(c);
				add(c);
				loadChongs(1);
			}

			c = null;
			for each(c in chongsOS)
			{
				c.y -= 250 * FP.elapsed;
				if (c.y < -c.getHeight())
				{
					c.x = FP.rand(C.SCREEN_WIDTH);
					c.y = C.SCREEN_HEIGHT;
				}
			}
		}

		override public function render():void
		{
			if (FP.random <= 0.2)
				title.scale += FP.random * 2 * ((FP.random <= 0.5) ? -1 : 1);
			if (Math.abs(title.scale) > 3)
				title.scale = 0.5;

			super.render();

			if (msg != null)
				msg.render();
		}
	}
}