package
{
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.Entity;
	import Muhniez;

	public class GameWorld extends World
	{
		private var m:Muhniez = null;
		private var man:Man = new Man();
		private var scoreboard:Text = new Text("$0", 0, 0, {align:"center", size:730});
		private var score:uint = 0;
		private var prevScore:int = -1;
		private var col1:uint = 0;
		private var col2:uint = 0;
		private var col3:uint = 0;
		private var chongs:Vector.<ChingChong> = new Vector.<ChingChong>();
		private var i:int = 0;
		private var j:int = 0;
		private var w:int = 0;
		private var c:ChingChong = null;

		public function GameWorld()
		{
			var i:int = 0;

			loadChongs();

			super();

			var sb:Entity = new Entity(0,0,scoreboard, null);
			sb.layer = 1000;
			add(sb);
			scoreboard.centerOrigin();

			for (i=0; i<C.BLOCKS_WIDE; ++i)
			{
				add(new Block(i*C.BLOCK_WIDTH,0));
				add(new Block(i*C.BLOCK_WIDTH,-C.STAGE_HEIGHT*C.BLOCK_HEIGHT-1));
			}

			for (i=-1; i>-C.STAGE_HEIGHT-1; --i)
			{
				add(new Block(0,i*C.BLOCK_HEIGHT));
				add(new Block((C.BLOCKS_WIDE-1)*C.BLOCK_WIDTH,i*C.BLOCK_HEIGHT));
			}

			for (i=-8; i>-C.STAGE_HEIGHT-1; i-=8)
			{
				addRow1(i);
			}

			var s:int = C.BLOCKS_WIDE/2-3;
			for (i=s; i<s+7; ++i)
			{
				add(new Blower(i*C.BLOWER_WIDTH,-1));
			}

			man.x = 100;
			man.y = -100;
			add(man);
		}

		public function getScore():uint
		{
			return score;
		}

		override public function update():void
		{
			super.update();

			m = man.collide("muhniez", man.x, man.y) as Muhniez;
			if (m != null && !m.isObtaining())
			{
				if (Mixer.SONG1.playing)
				{
					Mixer.fadeOut(Mixer.SONG1);
					Mixer.SONG2.loop();
				}
				Mixer.explode();
				m.obtain();
				prevScore = score;
				++score;
				man.speedUp();
				scoreboard.text = "$" + (score).toString();
				scoreboard.centerOrigin();

				for (i=0; i < V.getSeed(); ++i)
					chong();
			}
			V.manX = man.x;
			V.manY = man.y;


			FP.screen.color = V.col2;
			scoreboard.color = V.col1;
			FP.camera.x = man.x-((C.SCREEN_WIDTH/2)/FP.screen.scale);
			FP.camera.y = man.y-((C.SCREEN_HEIGHT/2)/FP.screen.scale);
			scoreboard.x = man.x;
			scoreboard.y = man.y-80;
			scoreboard.angle += V.n1;
		}

		override public function render():void
		{
			super.render();
		}

		private function addRow(rowStart:int=0, colStart:int=0, holeCol:int=-1, holeWidth:int=1, width:int=C.BLOCKS_WIDE-1, height:int=0):void
		{
			if (holeCol == -1)
			{
				for (j = rowStart; j <= rowStart+height; ++j)
				{
					for(i = colStart; i <= colStart+width; ++i)
					{
						add(new Block(i*C.BLOCK_WIDTH, j*C.BLOCK_HEIGHT));
						if (FP.random <= C.MUHNIEZ_PROB)
							add(new Muhniez(i*C.BLOCK_WIDTH+C.BLOCK_WIDTH/2,j*C.BLOCK_HEIGHT-C.MUHNIEZ_HEIGHT));
					}
				}
			}
			else
			{
				addRow(rowStart, colStart, -1, 1, holeCol, height);
				addRow(rowStart, holeCol+holeWidth+1, -1, 1, width-holeCol-holeWidth, height);
			}
		}

		private function addRow1(row:int=0):void
		{
			w = C.BLOCKS_WIDE-2-4;
			addRow(row, 4, w/3, (w/3)+1, w);
		}

		private function chong():void
		{
			c = chongs[FP.rand(chongs.length)];
			c.x = FP.camera.x + FP.rand(C.SCREEN_WIDTH);
			c.y = FP.camera.y + C.SCREEN_HEIGHT;
			add(c);
		}

		private function loadChongs():void
		{
			var i:int = 0;

			for (i=0; i<120; ++i)
			{
				chongs.push(new ChingChong(Japanese.random(), 0, 0));
			}
		}
	}
}